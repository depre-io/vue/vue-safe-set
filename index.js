(function () {
  'use strict';
  const Vue = require('vue');

  let vueSafeSet = function(obj, props, value) {
    if (typeof props == 'string') {
      props = props.split('.');
    }
    if (typeof props == 'symbol') {
      props = [props];
    }
    var lastProp = props.pop();
    if (!lastProp) {
      return false;
    }
    var thisProp;
    while ((thisProp = props.shift())) {
      if (typeof obj[thisProp] == 'undefined') {
        Vue.set(obj,thisProp,{});
      }
      obj = obj[thisProp];
      if (!obj || typeof obj != 'object') {
        return false;
      }
    }
    Vue.set(obj, lastProp, value);
    return true;
  }

  module.exports = vueSafeSet;
}());
